<?php

/**
 * Файл login.php для не авторизованного пользователя выводит форму логина.
 * При отправке формы проверяет логин/пароль и создает сессию,
 * записывает в нее логин и id пользователя.
 * После авторизации пользователь перенаправляется на главную страницу
 * для изменения ранее введенных данных.
 **/

// Отправляем браузеру правильную кодировку,
// файл login.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');
//Стартует новую сессию либо возобновляет существующую.(на идентификаторе сессии, переданном через Гет или Пост запросом либо переданный через куки.
// Начинаем сессию.
session_start();
//Если пользователь запустил сайт впервые, то session_start() устанавливает куки у клиента и создает врем.хранилище на сервере,
//связанное с идентификатором пользоавтеля
//определяет хранилище связанное с переданным текущим идентификатором
//Если в хранилище на сервере есть данные они помещаются в массив ССЕССИОН.
$messages = array();
// В суперглобальном массиве $_SESSION хранятся переменные сессии.
// Будем сохранять туда логин после успешной авторизации.
if (!empty($_SESSION['login'])) {
  // Если есть логин в сессии, то пользователь уже авторизован.
  // TODO: Сделать выход (окончание сессии вызовом session_destroy()
  //при нажатии на кнопку Выход).
  // Делаем перенаправление на форму.
  header('Location: ./');
}

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
?>
  <style>
    /* Сообщения об ошибках и поля с ошибками выводим с красным бордюром. */
    .formLogin{
      max-width: max-content;
    margin: 0 auto;
    margin-top: 20%;
    border-radius: 15px;
    padding: 10px;
    border: 1px;
    }
  </style>
<div class="formLogin">
<form action="" method="post">
  Логин
  <input name="login" id="login"  />
  Пароль
  <input name="pass" id="pass" />
  <input type="submit" value="Войти" />
</form>
</div>
<?php
}
// Иначе, если запрос был методом POST, т.е. нужно сделать авторизацию с записью логина в сессию.
else {
  try {
  $userBD = 'u26433';
  $passBD = '23464732';
  //PDO::ATTR_PERSISTENT => true) постоянные соединения
  $db = new PDO('mysql:host=localhost;dbname=u26433', $userBD, $passBD, array(PDO::ATTR_PERSISTENT => true));
  $logins= $_POST['login'];
  $pass= $_POST['pass'];
  $passMD5=md5($pass);
  $sql1 = 'SELECT * FROM users WHERE logins=?';
  $sql2 = 'SELECT * FROM users WHERE pass=?';
  $query = $db->prepare($sql1);
  $query->execute([$logins]);
  //fetch() возвращает строку результирующего набора
  $user= $query->fetch();
  $query2= $db->prepare($sql2);
  $query2->execute([$passMD5]);
  $user2= $query2->fetch();
  if ($user&&$user2) { 
    $_SESSION['login'] = $_POST['login'];
    // Записываем ID пользователя.
    $_SESSION['uid'] = 123;
    header('Location: ./');
  } else {
    print('<style>
    .bantik{
      margin: 0 auto;
      max-width: max-content;
    }
    .errorLogin {
      border: 2px solid red;
      max-width: max-content;
    margin: 0 auto;
    margin-top: 20%;
    }
    .messageErorLogin{
      max-width: max-content;
    }
    </style><div class="errorLogin"><div class="messageErorLogin">Такого логина нет или пароль неверный</div><div class="bantik"><input type="button" onclick="history.back();" value="Назад"/ ></div></div>');
  }
} catch (PDOException $e) {
  print('Error : ' . $e->getMessage());
  exit();
}
  // TODO: Проверть есть ли такой логин и пароль в базе данных.
  // Выдать сообщение об ошибках.

  // Если все ок, то авторизуем пользователя.

  // Делаем перенаправление.

}
